# IP Ranges Filter  :computer:

The idea of this project is to receive a `.txt` file full of IP ranges and then proccess the whole list to retrieve the fewest intervals possible.
<br><br>

**A generic entry example would be:**

```
1111111111-4444444444
2222222222-6666666666
8888888888-9999999999
```

**The response for this list after being filtered is:**

```
1111111111-6666666666
8888888888-9999999999
```

 
