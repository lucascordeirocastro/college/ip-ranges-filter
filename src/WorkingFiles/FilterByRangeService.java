package WorkingFiles;

import java.util.LinkedList;

class FilterByRangeService {

    public static LinkedList<IpRange> filterList(RedBlackTree<IpRange> redBlackTree) {
        LinkedList<IpRange> ipRangesList = redBlackTree.positionsCentral();
        LinkedList<IpRange> filteredList = new LinkedList<>();

        IpRange mainRange = ipRangesList.peekFirst();
        IpRange analysedRange = ipRangesList.pop();
        int aux = 0;

        while (!ipRangesList.isEmpty()) {

            assert mainRange != null;
            assert analysedRange != null;

            while (analysedRange.getInitialIp().compareTo(mainRange.getFinalIp() + 1) <= 0 && !ipRangesList.isEmpty()) {

                if (analysedRange.getFinalIp().compareTo(mainRange.getFinalIp()) > 0) {
                    mainRange.setFinalIp(analysedRange.getFinalIp());
                }

                if (ipRangesList.size() > 1) {
                    analysedRange = ipRangesList.pop();
                } else {
                    analysedRange = ipRangesList.peekFirst();
                    aux++;
                }

                if (aux > 1) {
                    ipRangesList.pop();
                }
            }

            filteredList.add(mainRange);

            if (!ipRangesList.isEmpty()) {
                mainRange = analysedRange;
            }
        }

        return filteredList;
    }

}
