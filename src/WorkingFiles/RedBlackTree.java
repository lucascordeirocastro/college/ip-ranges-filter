package WorkingFiles;

import java.util.LinkedList;
import java.util.Queue;

public class RedBlackTree<U extends Comparable<U>> {

    // ************************** Node *************************** \\
    private enum Color {BLACK, RED}

    public static class RedBlackNode<U extends Comparable<U>> {

        private Color color;
        private U value;
        private RedBlackNode<U> parent;
        private RedBlackNode<U> left;
        private RedBlackNode<U> right;
        public int numLeft = 0;
        public int numRight = 0;

        //Create Node
        public RedBlackNode(U value) {
            this();
            this.value = value;
        }

        //Constructor Nill
        public RedBlackNode() {
            color = Color.BLACK;
            numLeft = 0;
            numRight = 0;
            parent = null;
            left = null;
            right = null;
        }

        //Create and Set Value
        public U getValue() {
            return value;
        }

        public void setValue(U value) {
            this.value = value;
        }

        //Create, Set and Switch Color
        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            if (color == Color.BLACK) {
                this.color = Color.BLACK;
                return;
            }
            this.color = Color.RED;
        }

        public void switchColor() {
            if (getColor().equals(Color.RED)) {
                setColor(Color.BLACK);
            } else {
                setColor(Color.RED);
            }
        }

        //Create and Set Parent
        public RedBlackNode<U> getParent() {
            return parent;
        }

        public void setParent(RedBlackNode<U> parent) {
            this.parent = parent;
        }

        //Create and Set Left
        public RedBlackNode<U> getLeft() {
            return left;
        }

        public void setLeft(RedBlackNode<U> left) {
            this.left = left;
            left.setParent(this);
        }

        //Create and Set Left
        public RedBlackNode<U> getRight() {
            return right;

        }

        public void setRight(RedBlackNode<U> right) {
            this.right = right;
            right.setParent(this);
        }

        public String toString() {
            return "" + value;
        }


    }

    // ************************** Attributes *************************** \\
    private RedBlackNode<U> root;
    private RedBlackNode<U> nil = new RedBlackNode<U>();

    // ************************** Constructor ************************** \\
    public RedBlackTree() {
        root = nil;
        root.left = nil;
        root.right = nil;
        root.parent = nil;
    }

    // ************************** Methods ****************************** \\

    /**
     * Adiciona o objeto passado por parametro na arvore e arruma as referencias
     * para a quantidade de nodos a direira e a esquerda.
     * Notação: O(log n)
     *
     * @param newNode U - objeto
     */
    public void add(U newNode) {
        add(new RedBlackNode<U>(newNode));
    }

    private void add(RedBlackNode<U> newNode) {

        RedBlackNode<U> y = nil;
        RedBlackNode<U> x = root;

        while (!isNil(x)) {
            y = x;
            if (newNode.value.compareTo(x.value) < 0) {
                x.numLeft++;
                x = x.left;
            } else {
                x.numRight++;
                x = x.right;
            }
        }
        newNode.parent = y;

        if (isNil(y))
            root = newNode;
        else if (newNode.value.compareTo(y.value) < 0)
            y.left = newNode;
        else
            y.right = newNode;

        newNode.left = nil;
        newNode.right = nil;
        newNode.color = Color.RED;

        insertFixup(newNode);

    }

    /**
     * Remove o objeto passado por parametro na arvore.
     * Notação: O(log n)
     *
     * @param newNode U - objeto
     */
    public void remove(U newNode) {
        remove(new RedBlackNode<U>(newNode));
    }

    private void remove(RedBlackNode<U> v) {

        RedBlackNode<U> z = searchNodeRef(v.value);

        RedBlackNode<U> x = nil;
        RedBlackNode<U> y = nil;

        if (isNil(z.left) || isNil(z.right))
            y = z;

        else y = treeSuccessor(z);

        if (!isNil(y.left))
            x = y.left;
        else
            x = y.right;

        x.parent = y.parent;

        if (isNil(y.parent))
            root = x;

        else if (!isNil(y.parent.left) && y.parent.left == y)
            y.parent.left = x;

        else if (!isNil(y.parent.right) && y.parent.right == y)
            y.parent.right = x;

        if (y != z) {
            z.value = y.value;
        }

        fixNodeData(x, y);

        if (y.color == Color.BLACK)
            removeFixup(x);
    }

    /**
     * Retorna um booleano em relação a quantidade de nodos na arvore
     * Notação: O(1)
     *
     * @return boolean true se a arvore está vazia, false caso contrario
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Retorna o pai de um nodo
     * Notação: O(log n)
     *
     * @param o Recebe uma chave como parametro
     * @return nulo caso o nodo não esta na arvore, caso contrario retorna seu pai
     */
    public RedBlackNode<U> getParent(U o) {
        RedBlackNode<U> node = searchNodeRef(o);
        if (node == null) return null;
        return node.parent;
    }

    /**
     * Verifica se um elemento está na arvore
     * Notação: O(log n)
     *
     * @param o Recebe uma chave como parametro
     * @return boolean true se a chave foi encontrada, false caso contrario
     */
    public boolean contains(U o) {
        RedBlackNode<U> node = searchNodeRef(o);
        return node != null;
    }

    /**
     * Verifica a altura de uma arvore
     * Notação: O(n)
     *
     * @return int, retorna -1 caso a arvore esteja vazia, caso contrario retorna a altura da arvore
     */
    public int height() {
        if (isEmpty()) return -1;
        return heightAux(root) - 1;
    }

    private int heightAux(RedBlackNode<U> node) {
        if (isNil(node))
            return 0;
        else {
            /* compute the depth of each subtree */
            int leftDepth = heightAux(node.left);
            int rightDepth = heightAux(node.right);

            /* use the larger one */
            if (leftDepth > rightDepth)
                return (leftDepth + 1);
            else
                return (rightDepth + 1);
        }
    }

    /**
     * Retorna o clone de uma determinada arvore
     * Notação: O(n)
     *
     * @return RedBlackTree<U>, arvore identica a chamadora do metodo
     */
    public RedBlackTree<U> clone() {
        RedBlackTree<U> clone = new RedBlackTree<U>();
        cloneAux(root, clone);
        return clone;
    }

    private void cloneAux(RedBlackNode<U> n, RedBlackTree<U> clone) {
        if (!isNil(n)) {
            clone.add(n.getValue());
            cloneAux(n.left, clone);
            cloneAux(n.right, clone);
        }
    }

    /**
     * Verifica a quantidade de nodos de uma arvore
     * Notação: O(1)
     *
     * @return int, retorna -1 caso a arvore esteja vazia, caso contrario retorna a quantidade de nodos da arvore
     */
    public int size() {
        if (isEmpty()) return -1;
        return root.numLeft + root.numRight + 1;
    }

    // ************************** Walkways ************************** \\

    /**
     * Retorna uma lista com todos os elementos da arvore. Os elementos
     * sao colocados na lista seguindo um caminhamento pré fixado.
     * Notação: O(n)
     *
     * @return lista com os elementos da arvore na ordem pré fixado
     */
    public LinkedList<U> positionsPre() {
        LinkedList<U> list = new LinkedList<U>();
        if (!isEmpty()) positionsPreAux(list, root);
        return list;
    }

    private void positionsPreAux(LinkedList<U> list, RedBlackNode<U> node) {
        if (!isNil(node)) {
            list.add(node.getValue());
            positionsPreAux(list, node.left);
            positionsPreAux(list, node.right);
        }
    }

    /**
     * Retorna uma lista com todos os elementos da arvore. Os elementos
     * sao colocados na lista seguindo um caminhamento pos fixada.
     * Notação: O(n)
     *
     * @return lista com os elementos da arvore na ordem pos fixada
     */
    public LinkedList<U> positionsPos() {
        LinkedList<U> list = new LinkedList<U>();
        if (!isEmpty()) positionsPosAux(list, root);
        return list;
    }

    private void positionsPosAux(LinkedList<U> list, RedBlackNode<U> node) {
        if (!isNil(node)) {
            positionsPreAux(list, node.left);
            positionsPreAux(list, node.right);
            list.add(node.getValue());
        }
    }

    /**
     * Retorna uma lista com todos os elementos da arvore. Os elementos
     * sao colocados na lista seguindo um caminhamento central.
     * Notação: O(n)
     *
     * @return lista com os elementos da arvore na ordem central
     */
    public LinkedList<U> positionsCentral() {
        LinkedList<U> list = new LinkedList<U>();
        if (!isEmpty()) positionsCentralAux(list, root);
        return list;
    }

    private void positionsCentralAux(LinkedList<U> list, RedBlackNode<U> node) {
        if (!isNil(node)) {
            positionsCentralAux(list, node.left);
            list.add(node.getValue());
            positionsCentralAux(list, node.right);
        }
    }

    /**
     * Retorna uma lista com todos os elementos da arvore. Os elementos
     * sao colocados na lista seguindo um caminhamento por largura.
     * Notação: O(n)
     *
     * @return lista com os elementos da arvore na ordem de largura.
     */
    public LinkedList<U> positionsWidth() {
        LinkedList<U> largura = new LinkedList<U>();
        Queue<RedBlackNode<U>> fila = new LinkedList<>();

        fila.offer(root);

        while (!fila.isEmpty()) {
            if (!isNil(fila.peek().left)) fila.offer(fila.peek().left);
            if (!isNil(fila.peek().right)) fila.offer(fila.peek().right);
            largura.add(fila.poll().value);
        }

        return largura;


    }

    // ************************** Aux Methods ************************** \\

    /**
     * Apos a inserção de um nodo é necessario arrumar a arovre seguindo
     * os parametros da arovre rubro negra
     *
     * @param newNode referencia para o nodo a ser inserido na arvore
     */
    private void insertFixup(RedBlackNode<U> newNode) {

        RedBlackNode<U> y = nil;

        while (newNode.parent.color == Color.RED) {
            if (newNode.parent == newNode.parent.parent.left) {
                y = newNode.parent.parent.right;

                if (y.color == Color.RED) {
                    newNode.parent.color = Color.BLACK;
                    y.color = Color.BLACK;
                    newNode.parent.parent.color = Color.RED;
                    newNode = newNode.parent.parent;
                } else if (newNode == newNode.parent.right) {
                    newNode = newNode.parent;
                    leftRotate(newNode);
                } else {
                    newNode.parent.color = Color.BLACK;
                    newNode.parent.parent.color = Color.RED;
                    rightRotate(newNode.parent.parent);
                }
            } else {

                y = newNode.parent.parent.left;

                if (y.color == Color.RED) {
                    newNode.parent.color = Color.BLACK;
                    y.color = Color.BLACK;
                    newNode.parent.parent.color = Color.RED;
                    newNode = newNode.parent.parent;
                } else if (newNode == newNode.parent.left) {
                    newNode = newNode.parent;
                    rightRotate(newNode);
                } else {
                    newNode.parent.color = Color.BLACK;
                    newNode.parent.parent.color = Color.RED;
                    leftRotate(newNode.parent.parent);
                }
            }
        }

        root.color = Color.BLACK;

    }

    /**
     * Executa a rotação a esquerda
     *
     * @param x referencia para o nodo a ser rotado a esquerda na arvore
     */
    private void leftRotate(RedBlackNode<U> x) {

        leftRotateFixup(x);

        RedBlackNode<U> y;
        y = x.right;
        x.right = y.left;

        if (!isNil(y.left))
            y.left.parent = x;
        y.parent = x.parent;

        if (isNil(x.parent))
            root = y;

        else if (x.parent.left == x)
            x.parent.left = y;

        else
            x.parent.right = y;

        y.left = x;
        x.parent = y;
    }

    /**
     * Executa a rotação a direita
     *
     * @param y referencia para o nodo a ser rotado a direira na arvore
     */
    private void rightRotate(RedBlackNode<U> y) {

        rightRotateFixup(y);

        RedBlackNode<U> x = y.left;
        y.left = x.right;

        if (!isNil(x.right))
            x.right.parent = y;
        x.parent = y.parent;

        if (isNil(y.parent))
            root = x;

        else if (y.parent.right == y)
            y.parent.right = x;

        else
            y.parent.left = x;
        x.right = y;

        y.parent = x;

    }

    /**
     * Arruma as referencias para a quantidade de nodos a esquerda e a direita
     * após a rotação a esquerda
     *
     * @param node referencia para o nodo
     */
    private void leftRotateFixup(RedBlackNode<U> node) {

        if (isNil(node.left) && isNil(node.right.left)) {
            node.numLeft = 0;
            node.numRight = 0;
            node.right.numLeft = 1;
        } else if (isNil(node.left) && !isNil(node.right.left)) {
            node.numLeft = 0;
            node.numRight = 1 + node.right.left.numLeft + node.right.left.numRight;
            node.right.numLeft = 2 + node.right.left.numLeft + node.right.left.numRight;
        } else if (!isNil(node.left) && isNil(node.right.left)) {
            node.numRight = 0;
            node.right.numLeft = 2 + node.left.numLeft + node.left.numRight;

        } else {
            node.numRight = 1 + node.right.left.numLeft + node.right.left.numRight;
            node.right.numLeft = 3 + node.left.numLeft + node.left.numRight + node.right.left.numLeft + node.right.left.numRight;
        }

    }

    /**
     * Arruma as referencias para a quantidade de nodos a esquerda e a direita
     * após a rotação a direita
     *
     * @param node referencia para o nodo
     */
    private void rightRotateFixup(RedBlackNode<U> node) {

        if (isNil(node.right) && isNil(node.left.right)) {
            node.numRight = 0;
            node.numLeft = 0;
            node.left.numRight = 1;
        } else if (isNil(node.right) && !isNil(node.left.right)) {
            node.numRight = 0;
            node.numLeft = 1 + node.left.right.numRight + node.left.right.numLeft;
            node.left.numRight = 2 + node.left.right.numRight + node.left.right.numLeft;
        } else if (!isNil(node.right) && isNil(node.left.right)) {
            node.numLeft = 0;
            node.left.numRight = 2 + node.right.numRight + node.right.numLeft;

        } else {
            node.numLeft = 1 + node.left.right.numRight + node.left.right.numLeft;
            node.left.numRight = 3 + node.right.numRight + node.right.numLeft + node.left.right.numRight + node.left.right.numLeft;
        }

    }

    /**
     * Retorna m booleano acerca do nodo ser nil
     * Notação: O(1)
     *
     * @param node referencia para o nodo a ser conferido
     * @return boolean true se o nodo for nil, false caso contrario
     */
    private boolean isNil(RedBlackNode<U> node) {
        return node == nil;
    }

    /**
     * Procura um nodo com a value fornecida
     *
     * @param value referencia para o a chave a ser procurada
     * @return null se o nodo não for achado, retorna o nodo caso contrario
     */
    private RedBlackNode<U> searchNodeRef(U value) {
        RedBlackNode<U> current = root;

        while (!isNil(current)) {

            if (current.value.equals(value))
                return current;

            else if (current.value.compareTo(value) < 0)
                current = current.right;

            else
                current = current.left;
        }

        return null;
    }

    /**
     * Procura o sucessor de um nodo dado como parametro
     *
     * @param node referencia para o qual queremos achar o sucessor
     * @return o nodo sucessor do nodo informado
     */
    private RedBlackNode<U> treeSuccessor(RedBlackNode<U> node) {

        if (!isNil(node.left))
            return getSmallest(node.right);

        RedBlackNode<U> auxnode = node.parent;

        while (!isNil(auxnode) && node == auxnode.right) {
            node = auxnode;
            auxnode = auxnode.parent;
        }

        return auxnode;
    }

    /**
     * Retorna o menor valor, usando o nodo passado como parametro como raiz
     *
     * @param node referencia
     * @return o menor nodo achado
     */
    private RedBlackNode<U> getSmallest(RedBlackNode<U> node) {

        while (!isNil(node.left))
            node = node.left;
        return node;
    }

    /**
     * Arruma as referencias para a quantidade de nodos a esquerda e a direita
     * após a remoção do nado
     *
     * @param node referencia para o nodo a ser removido
     * @param x    valor da key que estava no node originalmente
     */
    private void fixNodeData(RedBlackNode<U> x, RedBlackNode<U> node) {

        RedBlackNode<U> current = nil;
        RedBlackNode<U> track = nil;

        if (isNil(x)) {
            current = node.parent;
            track = node;
        } else {
            current = x.parent;
            track = x;
        }

        while (!isNil(current)) {
            if (node.value != current.value) {
                if (node.value.compareTo(current.value) > 0)
                    current.numRight--;
                if (node.value.compareTo(current.value) < 0)
                    current.numLeft--;
            } else {
                if (isNil(current.left))
                    current.numLeft--;
                else if (isNil(current.right))
                    current.numRight--;
                else if (track == current.right)
                    current.numRight--;
                else if (track == current.left)
                    current.numLeft--;
            }

            track = current;
            current = current.parent;

        }

    }

    /**
     * Apos a remoção de um nodo é necessario arrumar a arovre seguindo
     * os parametros da arovre rubro negra
     *
     * @param node referencia para o nodo a ser removido na arvore
     */
    private void removeFixup(RedBlackNode<U> node) {

        RedBlackNode<U> auxnode;


        while (node != root && node.color == Color.BLACK) {

            if (node == node.parent.left) {
                auxnode = node.parent.right;
                if (auxnode.color == Color.RED) {
                    auxnode.color = Color.BLACK;
                    node.parent.color = Color.RED;
                    leftRotate(node.parent);
                    auxnode = node.parent.right;
                }

                if (auxnode.left.color == Color.BLACK &&
                        auxnode.right.color == Color.BLACK) {
                    auxnode.color = Color.RED;
                    node = node.parent;
                } else {
                    if (auxnode.right.color == Color.BLACK) {
                        auxnode.left.color = Color.BLACK;
                        auxnode.color = Color.RED;
                        rightRotate(auxnode);
                        auxnode = node.parent.right;
                    }
                    auxnode.color = node.parent.color;
                    node.parent.color = Color.BLACK;
                    auxnode.right.color = Color.BLACK;
                    leftRotate(node.parent);
                    node = root;
                }
            } else {

                auxnode = node.parent.left;

                if (auxnode.color == Color.RED) {
                    auxnode.color = Color.BLACK;
                    node.parent.color = Color.RED;
                    rightRotate(node.parent);
                    auxnode = node.parent.left;
                }
                if (auxnode.right.color == Color.BLACK &&
                        auxnode.left.color == Color.BLACK) {
                    auxnode.color = Color.RED;
                    node = node.parent;
                } else {
                    if (auxnode.left.color == Color.BLACK) {
                        auxnode.right.color = Color.BLACK;
                        auxnode.color = Color.RED;
                        leftRotate(auxnode);
                        auxnode = node.parent.left;
                    }
                    auxnode.color = node.parent.color;
                    node.parent.color = Color.BLACK;
                    auxnode.left.color = Color.BLACK;
                    rightRotate(node.parent);
                    node = root;
                }
            }
        }
        node.color = Color.BLACK;
    }


}
