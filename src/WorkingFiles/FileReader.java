package WorkingFiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReader {
    public RedBlackTree<IpRange> readFile(String archive) {
        RedBlackTree<IpRange> redBlackTree = new RedBlackTree<IpRange>();

        Path path1 = Paths.get(archive);

        try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
            String line = null;

            while ((line = reader.readLine()) != null) {
                String[] items = line.split("-");
                IpRange iprange = new IpRange(Integer.parseInt(items[0]), Integer.parseInt(items[1]));
                redBlackTree.add(iprange);
            }
        } catch (IOException x) {
            System.err.format("Erro de E/S: %s%n", x);
            return null;
        }
        return redBlackTree;
    }


}
