package WorkingFiles;
public class IpRange implements Comparable<IpRange> {

    private Integer InitialIp;
    private Integer FinalIp;

    //Constructor
    public IpRange(Integer IntialIp, Integer FinalIp) {
        this.InitialIp = IntialIp;
        this.FinalIp = FinalIp;
    }

    //Getters and Setters
    public Integer getInitialIp() {return InitialIp;}
    public Integer getFinalIp() {return FinalIp;}
    public void setInitialIp(Integer IntialIp) { this.InitialIp = IntialIp; }
    public void setFinalIp(Integer FinalIp) { this.FinalIp = FinalIp;}

    //CompareTo e ToString dus Guri
    @Override
    public int compareTo(IpRange another) { return InitialIp.compareTo(another.getInitialIp());}
    @Override
    public String toString() { return "\nFrom " + InitialIp + " To " + FinalIp; }

}