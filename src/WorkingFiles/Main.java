package WorkingFiles;

public class Main {
    public static void main(String[] args) {
        FileReader fileReader = new FileReader();

        System.out.println("Start");

        RedBlackTree<IpRange> testList = fileReader.readFile("src/WorkingFiles/TestCases/testExample01.txt");

        System.out.println(FilterByRangeService.filterList(testList));

        System.out.println("\nEnd");
    }
}
